function [cost, grad] = softmaxCost(theta, numClasses, inputSize, lambda, data, labels)

% numClasses - the number of classes 
% inputSize - the size N of the input vector
% lambda - weight decay parameter
% data - the N x M input matrix, where each column data(:, i) corresponds to
%        a single test set
% labels - an M x 1 matrix containing the labels corresponding for the input data
%

% Unroll the parameters from theta
theta = reshape(theta, numClasses, inputSize);

numCases = size(data, 2);

groundTruth = full(sparse(labels, 1:numCases, 1));
cost = 0;

thetagrad = zeros(numClasses, inputSize);

%% ---------- YOUR CODE HERE --------------------------------------
%  Instructions: Compute the cost and gradient for softmax regression.
%                You need to compute thetagrad and cost.
%                The groundTruth matrix might come in handy.

% calculate the p(y(i) = j | x(i) ; theta)
linear_theta = theta * data;
linear_theta_recentered = bsxfun(@minus, linear_theta, max(linear_theta, [], 1));
theta_exp = exp(linear_theta_recentered);

h_theta = bsxfun(@rdivide, theta_exp, sum(theta_exp, 1) );

cost_theta = groundTruth .* max(log(h_theta), -1000);

weight_decay_term = 0.5 * lambda * sum(sum(theta.^2));
cost = -1 * sum(sum(cost_theta)) ./ numCases + weight_decay_term;
  
weight_decay_grad_term = lambda * theta;
thetagrad = -1 * (groundTruth - h_theta) * data' ./ numCases + weight_decay_grad_term;

% ------------------------------------------------------------------
% Unroll the gradient matrices into a vector for minFunc
grad = [thetagrad(:)];

end

